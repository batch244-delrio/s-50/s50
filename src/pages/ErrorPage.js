// import React from 'react';
// import { Link } from 'react-router-dom';
// import Banner from '../components/Banner';

// export default function ErrorPage () {
//   return (
//     <Banner 
//       title="Page not found!"
//       message={<>Go back to <Link to="/">homepage</Link>.</>}
//       showButton={false}
//     />

//   );
// };

 

import Banner from '../components/Banner';

export default function Error() {

  const data = {
    title : "404 - Not Found",
    content : "The page you are looking for cannot be found.",
    destination: "/",
    label: "Back home"
  }

  return (
    <Banner data={data}/>
  )
}