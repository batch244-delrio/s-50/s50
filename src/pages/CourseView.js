import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function CourseView() {

    // The "useParams" hook allows us to retrieve the courseId passed via URL
    const {courseId} = useParams();

    // To be able to obtain user ID so that we can Enroll a user
    const {user} = useContext(UserContext);

    const [ name, setName ] = useState('');
    const [ description, setDescription ] = useState('');
    const [ price, setPrice ] = useState(0);

    useEffect(() => {
        console.log(courseId);

        // fetch request that willl retrieve the details of the course of the database to be displayed in the CourseView page
        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
        .then (res => res.json())
        .then(data => {
            console.log(data)

            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
        })
    }, [courseId])

    const enroll = (courseId) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/enroll`,{
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                courseId: courseId
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data === true) {
                Swal.fire({
                    Title: "Successfully Enrolled",
                    icon: "success",
                    text: "You have successfully enrolled sa panibagong gastos!"
                })
            }else {
                Swal.fire({
                    Title: "Something went wrong",
                    icon: "error",
                    text: "Please try again!"
                })
            }
        })
    }

    return (

        <Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3}}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price</Card.Subtitle>
                            <Card.Text>Php {price}</Card.Text>
                            <Card.Subtitle>Class Schedule</Card.Subtitle>
                            <Card.Text>8 am - 5 pm</Card.Text>
                            
                            {user.id !== null
                                ?
                                <Button variant="primary" onClick = {() => enroll(courseId)}>Enroll</Button>
                                :
                                <Button as={Link} to="/login" variant="danger">Log in to Enroll</Button>
                            }

                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}