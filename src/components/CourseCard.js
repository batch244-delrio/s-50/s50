import { Card, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

export default function CourseCard({courseProp}) {

	const { _id, name, description, price } = courseProp;

	// // Syntax:
	// // Conts [getter, setter] = useState(initialGetterValue)
	// const [ count, setCount ] = useState(1);

	// // UseState hook for getting and setting the seats for the course
	// const [ seats, setSeats ] = useState(30)

	// const [ isOpen, setIsOpen ] = useState(false)

	// // Function that keeps track of the enrollees for a course
	// 	function enroll() {
	// 	// if (seats >0) {
	// 		setSeats(seats - 1);
	// 		setCount(count + 1);
	// 		console.log(`Enrollees:  ${count}`);
		// } else {
		// 	return alert("No more Seats");
		// 	}
		// }

		// Define a "useEffect" hook to have the "CourseCard" component do perform a certain task after every DOM update
            // This is run automatically both after initial render and for every DOM update
    	    // React will re-run this effect ONLY if any of the values contained in the seats array has changed from the last render / update
		
		// useEffect(() => {
		// 	if(seats === 0) {
		// 		setIsOpen(true);
		// 	}
		// }, [seats]);

    return (

	    <Card>
	        <Card.Body>
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>Php {price}</Card.Text>
	            {/*<Card.Text>Seats: {seats}</Card.Text*/}
	            <Button as={Link} variant="primary" to={`/courses/${_id}`}>Details</Button>
	        </Card.Body>
	    </Card>  

    )
}
