// import { Button, Row, Col } from 'react-bootstrap';


// export default function Banner({title, message, showButton = true }) {
//   return (
//     <Row>
//       <Col className="p-5 text-center">
//         <h1>{title}</h1>
//         <p>{message}</p>
//         {showButton ? <Button variant="primary">Enroll Now!</Button> : null}
//       </Col>
//     </Row>
//   );
// }

import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {

  const { title, content, destination, label } = data;

  return (
    <Row>
      <Col className="p-5 text-center">
        <h1>{title}</h1>
        <p>{content}</p>
        <Button as={Link} to={destination} variant="primary">{label}</Button>
      </Col>
    </Row>
  )
}